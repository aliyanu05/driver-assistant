#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <I2C.h>

#define    LIDARLite_ADDRESS   0x62          // Default I2C Address of LIDAR-Lite.
#define    RegisterMeasure     0x00          // Register to write to initiate ranging.
#define    MeasureValue        0x04          // Value to initiate ranging.
#define    RegisterHighLowB    0x8f          // Register to get both High and Low bytes in 1 call.

static const int RXPin = 12, TXPin = 10;
static const uint32_t GPSBaud = 115200;

// The TinyGPS++ object
TinyGPSPlus gps;
float latitude=0.0, longitude=0.0;
int spd, distance;
String ket;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

void setup()
{
  Serial.begin(9600);
  ss.begin(GPSBaud);
  I2c.begin(); // Opens & joins the irc bus as master
  delay(100); // Waits to make sure everything is powered up before sending or receiving data  
  I2c.timeOut(50); // Sets a timeout to ensure no locking up of sketch if I2C communication fails
  

}

void loop()
{
  // This sketch displays information every time a new sentence is correctly encoded.
  while (ss.available() > 0)
    if (gps.encode(ss.read()))
      displayInfo();
      lidar();

      //Sending with Serial Com
      Serial.print(spd);
      Serial.print(",");
      Serial.print(distance);
      Serial.print(",");
      Serial.print(latitude,7);
      Serial.print(",");   
      Serial.print(longitude,7); 
      Serial.println();         

 delay(100);
}

void displayInfo()
{
  if (gps.location.isValid())
  {
    latitude =  gps.location.lat();
    longitude = gps.location.lng();
  }

  if (gps.speed.isValid())
  {
    spd = gps.speed.kmph();
  }
  

}

void lidar(){
      // Write 0x04 to register 0x00
      uint8_t nackack = 100; // Setup variable to hold ACK/NACK resopnses     
      
      while (nackack != 0){ // While NACK keep going (i.e. continue polling until sucess message (ACK) is received )
        nackack = I2c.write(LIDARLite_ADDRESS,RegisterMeasure, MeasureValue); // Write 0x04 to 0x00
        delay(1); // Wait 1 ms to prevent overpolling
      }
    
      byte distanceArray[2]; // array to store distance bytes from read function
      
      // Read 2byte distance from register 0x8f
      nackack = 100; // Setup variable to hold ACK/NACK resopnses     
      while (nackack != 0){ // While NACK keep going (i.e. continue polling until sucess message (ACK) is received )
        nackack = I2c.read(LIDARLite_ADDRESS,RegisterHighLowB, 2, distanceArray); // Read 2 Bytes from LIDAR-Lite Address and store in array
        delay(1); // Wait 1 ms to prevent overpolling
      }
      
      distance = (distanceArray[0] << 8) + distanceArray[1];  // Shift high byte [0] 8 to the left and add low byte [1] to create 16-bit int

//      if(distance>10){
//          Serial.println(distance);
//      }else{
//          Serial.println("900");
//      }

        
      // return Distance
      return distance;
}
