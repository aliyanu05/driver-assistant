import colorama
import os
import serial
import serial.tools.list_ports# for viewing serialports
import time
import sys


class Arduino:
	colorama.init()
	def __init__(self,baudrate,esc,comPort):
		self.baud=baudrate
		self.connected=False
		self.connection=serial.Serial()
		self.__connect(comPort)
		self.escChar=esc
	
	def __connect(self,comPort):
		try:
			self.connection.baudrate=self.baud
			self.connection.timeout=1
			self.connection.port=comPort
			self.connection.open()
			time.sleep(2)
			if self.connection.is_open:
				print ("Arduino is connected !",'green',end="\n")
				self.connected=True
				time.sleep(2)
				return True
		except serial.SerialException:
			print("Please enter a valid port",'red',end="\n")
			return False
		
	def serRead(self):
		rsp=self.connection.readline()
		strRsp=""
		for i in rsp:
			strRsp=strRsp+chr(i)
		return strRsp[0:len(strRsp)-2]
		
	
	def getConnStatus(self):
		if self.connection.is_open:
			return True
		return False	
	
	def closePort(self):
		if self.connection.is_open:
			self.connection.close()
	

	
	


	
	
	
	
	
	
	
	
	
	
	
	
	
