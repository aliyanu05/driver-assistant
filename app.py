import arduinoSerial
import time
import atexit
import engine 
import threading
import serviceCSV
import socket
from flaskwebgui import FlaskUI

# import fuzzy
from flask import Flask,request, redirect, url_for, send_from_directory, render_template
app = Flask(__name__,static_url_path='')
ui = FlaskUI(app) 

@app.before_first_request
def setupSerial():
	global arduino
	arduino=arduinoSerial.Arduino(9600,'*',"COM31")
		
@app.route("/")
def page():
	if arduino.getConnStatus()==True:
		return app.send_static_file('index.html')

@app.route("/data_pelanggaran")
def showPelanggaran():
		return app.send_static_file('data_pelanggaran.html')


@app.route('/state')	
def getState():
	data=arduino.serRead()
	result = engine.engine_process(data)
	print(result)
	return result

@atexit.register
def disconnect():
	arduino.closePort()
	print('Port closed !!!')
	
	
if __name__ == "__main__":
	# app.run(debug=True,host='127.0.0.1')
	ui.run()


