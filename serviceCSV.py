import csv
import pandas as pd

def addTo_data_uji(Id,speed,distance,period,warning,label1,lng,lat,warning_value,label2):
        file = open('./engine/dataset/new_dataset_uji.csv', 'a', newline='\n')
        barisbaru = [Id,speed,distance,period,warning,label1,lng,lat,warning_value,label2]
        filecsv = csv.writer(file)
        filecsv.writerow(barisbaru)

def addTo_data_temp(speed,dis,period,warning):
        file = open('./engine/dataset/data_temp.csv', 'w')
        barisbaru = [speed,dis,period,warning]
        filecsv = csv.writer(file)
        filecsv.writerow(barisbaru)

def addTo_data_pelanggaran(id,date,spd,spd_sign,lat,long,ket):
        file = open('./static/pelanggaran.csv', 'a', newline='\n')
        barisbaru = [id,date,spd,spd_sign,lat,long,ket]
        filecsv = csv.writer(file)
        filecsv.writerow(barisbaru)

def searchFromCSV(spd,dst,period,warning):
    label = 404
    csv_file = csv.reader(open('./engine/dataset/data_full.csv','r'))
    for row in csv_file:
        if str(spd) in row[1] and str(dst) in row[2] and str(period) in row[3] and str(warning) in row[4] :
            label = row[6]     
    
    files = csv.reader(open('./engine/dataset/new_dataset_uji.csv','r'))    
    for row in files:
        if str(spd) in row[1] and str(dst) in row[2] and str(period) in row[3] and str(warning) in row[4] :
            label = row[5]     
 
    if label==404:
            label==404
            
#     print(label)
    return label


def addHeader():
        with open('./engine/dataset/data_temp.csv',newline='') as f:
            r = csv.reader(f)
            data = [line for line in r]
        with open('./engine/dataset/data_temp.csv','w',newline='') as f:
            w = csv.writer(f)
            w.writerow(['speed','distance','period','warning','label'])
            w.writerows(data)

def readKlasifikasiSVM():
        data=pd.read_csv('./engine/dataset/new_dataset_uji.csv', usecols=['Id','speed','distance','warning','label1'])
        print(data)


def readFuzzy():
        data=pd.read_csv('./engine/dataset/rekomendasi_kecepatan.csv')
        print(data)

