import rambu
import fuzzy
import svm_calculate
import serviceCSV
import csv


def engine_process(data):
    dt = data.split(',')
    speed = int(dt[0])
    dis = int(dt[1])
    lat = str(dt[2])
    lng = str(dt[3])
    r = rambu.deteksi_rambu().split(',') #warning : status,value
    
    warning = str(r[0])
    warning_value = str(r[1])
    period = str(r[2])
   
    label2 = cekPelanggaran(dis, warning_value)

    label1 = serviceCSV.searchFromCSV(speed,dis,period,warning)

   
    if label1 == 404:
        serviceCSV.addTo_data_temp(speed,dis,period,warning)       # Replace data_temp.csv
        serviceCSV.addHeader()                              # Add Header data_temp.csv
        label1 = svm_calculate.Fungsi_SVM()  # Get Label from SVM function
        Id = len(list(csv.reader(open('./dataset/new_dataset_uji.csv'))))
        serviceCSV.addTo_data_uji(str(Id),str(speed),str(dis),period,warning,str(label1),lat,lng,warning_value,str(label2))
    
    fuzzyData = fuzzy.fuzzy_calculate(speed,dis).split(',')  # fuzzyData : speed_recomendation, action
    speed_recomendation = fuzzyData[0]
    action = fuzzyData[1]

    result = str(speed) +","+ str(dis) +","+ lat +","+ lng +","+ warning +","+ warning_value +","+ str(speed_recomendation) +","+ action +","+ str(label1) +","+ str(label2)
    print(result)
    return result

def cekPelanggaran(distance, warning_value):
    if type(warning_value) is int :
        if distance>warning_value:
            label=1
        else:
            label=0
    else:
        label=0
    return label

if __name__ == '__main__':
    server_program()
# engine_process('50,39,-7.23434,108.2479234')