import pandas as pd  
import numpy as np  
import csv


def Fungsi_SVM():

    data_train=pd.read_csv('./dataset/data_train.csv', usecols=['speed','distance','warning','label'])
    data_uji=pd.read_csv('./dataset/data_temp.csv', usecols=['speed','distance','warning','label'])
    X_train = data_train.drop('label', axis=1)  
    y_train = data_train['label'] 
    X_test  = data_uji.drop('label', axis=1)  
    # y_test = data_uji['label']

    from sklearn.svm import SVC  
    svclassifier = SVC(kernel='rbf', C = 10.0, gamma=0.1)  
    svclassifier.fit(X_train, y_train) 

    #  Prediction
    y_pred = svclassifier.predict(X_test) 

    label = "".join(str(e) for e in y_pred)   
    
    # Evaluation
    # from sklearn.metrics import classification_report, confusion_matrix  
    # print(confusion_matrix(y_test, y_pred))  
    # print(classification_report(y_test, y_pred))
    # print(label)
    return label



