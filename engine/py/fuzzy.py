
#Fuzzy Logic untuk penentuan penurunan dan kenaikan kecepatan angkot
#Kategori kecepatan
#Cepat = >60
#Sedang = >30 && <60
#Lambat = <30


import csv

def fuzzy_calculate(speed,dis):
    
    # d = data.split(',')
    # speed=int(d[0])
    # dis=int(d[1])
    #Inferensi
    #Inferensi Kecepatan
    #Low Speed
    if speed>=30:
        low_speed=0
    elif speed<20:
        low_speed=1
    elif 20<=speed and speed<30:
        low_speed=(30-speed)/10

    #High Speed
    if speed<=40:
        high_speed=0
    elif speed>60:
        high_speed=1
    elif 40<speed and speed<=60:
        high_speed=(60-speed)/20

    #Medium Speed
    if speed <=20:
        medium_speed=0
    elif speed>60:
        medium_speed=0
    elif 20<speed and speed<=40:
        medium_speed=(speed-20)/20
    elif 40<speed and speed<60:
        medium_speed=(60-speed)/20


    #Inferensi Jarak
    #Jarak Dekat
    if dis>=30:
        dis_dekat = 0
    elif dis<10:
        dis_dekat = 1
    elif 10 <= dis and dis < 30:
        dis_dekat = (30-dis)/20

    #Jarak  JAUH
    if dis<=30:
        dis_jauh = 0
    elif dis>50:
        dis_jauh = 1
    elif 30 < dis and dis <= 50:
        dis_jauh = (50-dis)/20

    #Jarak SEDANG
    if dis<=10:
        dis_sedang = 0
    elif dis>=50:
        dis_sedang = 0
    elif 10 < dis and dis <= 30:
        dis_sedang = (dis-10)/20
    elif 30 < dis and dis < 50:
        dis_sedang = (50-dis)/20

    #RULE

    # RULE

    # RULE 1
    #IF	Kecepatan=Rendah AND Jarak=Dekat THEN Aksi=Turunkan Kecepatan
    a1 = min(low_speed,dis_dekat)
    z1 = 60 - (a1*30)
    

    # RULE 2
    #IF	Kecepatan=Rendah AND Jarak=Sedang THEN Aksi=Naikan Kecepatan
    a2 = min(low_speed,dis_sedang)
    z2 = 30 + (a2*30)
    

    # RULE 3
    #IF	Kecepatan=Rendah AND Jarak=Jauh THEN Aksi=Naikan Kecepatan
    a3 = min(low_speed,dis_jauh)
    z3 = 30 + (a3*30)

    # RULE 4
    #IF	Kecepatan=Sedang AND Jarak=Dekat THEN Aksi=Turunkan Kecepatan
    a4 = min(medium_speed,dis_dekat)
    z4 = 60 - (a4*30)

    # RULE 5
    #IF	Kecepatan=Sedang AND Jarak=Sedang THEN Aksi=Turunkan Kecepatan
    a5 = min(medium_speed,dis_sedang)
    z5 = 60 - (a5*30)

    # RULE 6
    #IF	Kecepatan=Sedang AND Jarak=Jauh THEN Aksi=Naikan Kecepatan
    a6 = min(medium_speed,dis_jauh)
    z6 = 30 + (a6*30)

    # RULE 7
    #IF	Kecepatan=Tinggi AND Jarak=Dekat THEN Aksi=Turunkan Kecepatan
    a7 = min(high_speed,dis_dekat)
    z7 = 60 - (a7*30)

    # RULE 8
    # IF	Kecepatan=Tinggi AND Jarak=Sedang THEN Aksi=Turunkan Kecepatan
    a8 = min(high_speed,dis_sedang)
    z8 = 60 - (a8*30)

    # RULE 9
    #IF	Kecepatan=Tinggi AND Jarak=Jauh THEN Aksi=Naikan Kecepatan
    a9 = min(high_speed,dis_jauh)
    z9 = 30 + (a9*30)

    # DEFUZZIFIKASI
    Z_Total = ((a1*z1) + (a2*z2) + (a3*z3) + (a4*z4) + (a5*z5) + (a6*z6) + (a7*z7) + (a8*z8) + (a9*z9)) / (a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9)

    # print("a1 = ",a1,"z1 =",z1)
    # print("a2 = ",a2,"z2 =",z2)
    # print("a3 = ",a3,"z3 =",z3)
    # print("a4 = ",a4,"z4 =",z4)
    # print("a5 = ",a5,"z5 =",z5)
    # print("a6 = ",a6,"z6 =",z6)
    # print("a7 = ",a7,"z7 =",z7)
    # print("a8 = ",a8,"z8 =",z8)
    # print("a9 = ",a9,"z8 =",z9)
    
    if a2>0 or a3>0 or a6>0 or a9>0 or a5>0 :
        # print("Naikan Kecepatan Hingga ", Z_Total)
        label = "Naikkan Kecepatan"
    else:
        # print("Turunkan Kecepatan Hingga ", Z_Total)
        label = "Turunkan Kecepatan"
    data = str(Z_Total)+","+label
    # print(data)
    return data
    # file = open('dataset/Pengujian_Fuzzy.csv', 'a', newline='\n')
    # barisbaru = [speed,dis,label,Z_Total]
    # filecsv = csv.writer(file)
    # filecsv.writerow(barisbaru)

# fuzzy_calculate(40,40)


