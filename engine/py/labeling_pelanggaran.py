from sklearn import svm
import matplotlib.pyplot as plt
import numpy as np


X = [[0, 0], [10, 10],[20,30],[30,30],[40, 30], [80,60], [80,50]]
y = [0, 1, 2, 3, 4, 5, 6]
clf = svm.SVC() 
clf.fit(X, y)

x_pred = [[10,10]]
x_pred = [[10,10]]
p = np.array(clf.decision_function(X)) # decision is a voting function
prob = np.exp(p)/np.sum(np.exp(p),axis=1) # softmax after the voting
classes = clf.predict(X)

_ = [print('Sample={}, Prediction={},\n Votes={} \nP={}, '.format(idx,c,v, s)) for idx, (v,s,c) in enumerate(zip(p,prob,classes))]